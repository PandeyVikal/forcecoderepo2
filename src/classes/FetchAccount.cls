public with sharing class FetchAccount {
    @AuraEnabled
    public static list<Account> getAccounts(){
        
        List<Account> lstAcc = [Select Id, name, industry from Account];
        
        return lstAcc;
    }
}