global class CalculateInterest implements Database.Batchable<sObject> {
   
   global Decimal Value;
   global List<Loan__c> ln = new List<Loan__c>();

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator('SELECT Id, Principle_Amount__c, Rate_of_Interest__c, CreatedDate, Interest__c, Amount__c FROM Loan__c');
   }

   global void execute(Database.BatchableContext BC, List<Loan__c> scope){
       
      //List<Loan__c> ln = new List<Loan__c>();
      
      for(Loan__c s : scope)
      {
          Date startDate = date.newInstance(s.CreatedDate.year(), s.CreatedDate.month(), s.CreatedDate.day());
          Date endDate = Date.today();
          Value = (s.Principle_Amount__c * s.Rate_of_Interest__c * (startDate.daysBetween(endDate.addYears(2))/365))/100.00;
          //s.put('Interest__c',Value); 
          
          s.Interest__c=Value;
          ln.add(s);
          
      }      
       //update scope;   
       update ln;
       
       /*
       //=================Task Generation=======
       List<Task> taskList = new List<Task>();
       
       for(Loan__c l : ln){
            taskList.add(new Task(Subject='Interest Calculation Completed', ActivityDate=Date.today(), WhatId=l.Id));
        }
        insert taskList;*/
   }                

   global void finish(Database.BatchableContext BC){
            
       List<Task> taskList = new List<Task>();
       
       for(Loan__c l : [SELECT ID, Amount__c,Interest__c, Principle_Amount__c,Rate_of_Interest__c FROM Loan__c]){
                taskList.add(new Task(Subject='Interest Calculation Completed', ActivityDate=Date.today(), WhatId=l.Id));
        }
        insert taskList;
   }

}